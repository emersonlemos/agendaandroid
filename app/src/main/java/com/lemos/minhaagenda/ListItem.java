package com.lemos.minhaagenda;

/**
 * Created by Emerson on 15/03/2017.
 */

public class ListItem {

    private String name;
    private String number;

    public ListItem(String name, String number) {
        this.name = name;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}

