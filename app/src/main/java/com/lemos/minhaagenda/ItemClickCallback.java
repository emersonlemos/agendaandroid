package com.lemos.minhaagenda;

/**
 * Created by Emerson on 15/03/2017.
 */

public interface ItemClickCallback {
    void onIconClick(int p);
}
