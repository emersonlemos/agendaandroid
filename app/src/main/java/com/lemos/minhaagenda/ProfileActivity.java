package com.lemos.minhaagenda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ProfileActivity extends AppCompatActivity {
    private EditText etName;
    private String getName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        etName = (EditText) findViewById(R.id.et_name);
    }

    public void saveUpdates (View v) {
        getName = etName.getText().toString();
        Intent i = new Intent(ProfileActivity.this, MainActivity.class);
        i.putExtra("name", getName);
        startActivity(i);
        finish();
    }
}
