package com.lemos.minhaagenda;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ItemClickCallback {



//    RECYCLEVIEW DECLARATIONS
    private RecyclerView recyclerView;
    private MyAdapter adapter;
    private List<ListItem> listItems;
    private static final String[] names = {
            "Satanás",
            "Mochila de criança",
            "Cramulhão",
            "Minha sogra",
            "Walace",
            "Belzebu",
            "Xuxa",
            "Zé do caixão"
    };
    private static final String[] numbers = {
            "83996558888",
            "83996552342",
            "83996550304",
            "83991230000",
            "83996340023",
            "83996550000",
            "83996340023",
            "83996550000"
    };
    //    END - RECYCLEVIEW DECLARATIONS

    private TextView tv_name;
    private String nameUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);




        //    RECYCLEVIEW - CRIANDO E INFLANDO
        recyclerView = (RecyclerView) findViewById(R.id.rv_contacts);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        listItems = new ArrayList<>();

        for (int i = 0; i <= 7; i++) {
            ListItem listItem = new ListItem(names[i], numbers[i]);
            listItems.add(listItem);
        };

        adapter = new MyAdapter(listItems, MainActivity.this);


        //  Seto meu listener nesse contexto
        adapter.setItemClickCallback(MainActivity.this);

        recyclerView.setAdapter(adapter);
        //    END - RECYCLEVIEW - CRIANDO E INFLANDO


//        RECUPERANDO NOME DO PERFIL NO SIDEBAR
        View headerView = navigationView.getHeaderView(0);
        tv_name = (TextView) headerView.findViewById(R.id.tv_name);





    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    public void goToProfile (View v) {
        Intent i = new Intent(MainActivity.this, ProfileActivity.class);
        startActivity(i);
    }

    @Override
    protected void onResume() {
        super.onResume();

//        Modifica nome no sidebar com base no valor passado pela activity anterior
        if (getIntent().getExtras() != null) {
            String nameProfile = getIntent().getExtras().getString("name");
            tv_name.setText(nameProfile);

        }
    }

    @Override
    public void onIconClick(int p) {

        //  Recupera a posição do item no RecyclerView
        ListItem contact = (ListItem) listItems.get(p);

        //  Realiza uma discagem pegando o telefone, com base na posição da lista
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contact.getNumber()));
        startActivity(intent);

        //  Executa alguma ação ou evento no meu adapter
        adapter.notifyDataSetChanged();
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_config) {
            Intent i = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_web) {
            Intent i = new Intent(MainActivity.this, WebActivity.class);
            startActivity(i);

        } else if (id == R.id.nav_sair) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
