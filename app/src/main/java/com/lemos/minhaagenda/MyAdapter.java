package com.lemos.minhaagenda;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Emerson on 15/03/2017.
 */

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{

    private List<ListItem> listItems;
    private Context context;
    private ItemClickCallback itemClickCallback;

    public void setItemClickCallback(final ItemClickCallback itemClickCallback) {
        this.itemClickCallback = itemClickCallback;
    }


    public MyAdapter(List<ListItem> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contact_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ListItem listItem = listItems.get(position);

        holder.textViewName.setText(listItem.getName());
        holder.textViewNumber.setText(listItem.getNumber());
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView textViewName;
        public TextView textViewNumber;
        private ImageView iv_call;
        private View container;

        public ViewHolder (View itemView) {
            super(itemView);

            textViewName = (TextView) itemView.findViewById(R.id.tv_name);
            textViewNumber = (TextView) itemView.findViewById(R.id.tv_number);
            iv_call = (ImageView) itemView.findViewById(R.id.iv_call);
            container = itemView.findViewById(R.id.cont_item_root);
            iv_call.setOnClickListener(this);
        }

        public void onClick(View view) {
            if(view.getId() == R.id.iv_call){
                //  Pega o id da posição do item clicado no meu adapter
                itemClickCallback.onIconClick(getAdapterPosition());
            }
        }


    }
}
